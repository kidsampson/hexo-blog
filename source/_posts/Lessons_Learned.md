---
title: Lessons Learned
tags:
---
Setting up this website was a bit more difficult than I was expecting but I learned a lot along the way.

First thing I learned was to not use [outdated tutorials](http://chitrangshah.com/2014/03/13/static-blog-site-with-hexo-part-3/) to try and set something up. I spent a least an hour fiddling with grunt-s3 only to come realize it is no longer maintained. (Really my fault for not following the link to the project page where it says it in big bold letters.)

So once I realized that was my issue I quickly rewrote my code but still had a few errors, luckily found this [blog post](http://ojisanseiuchi.com/2016/02/25/Automate-hexo-blogging-tasks-with-Grunt/) which helped clear up my mistakes.

### Conclusion

I want to give credit to both [Chitrang Shah](http://chitrangshah.com/) and [Alan Duncan](http://ojisanseiuchi.com/), as without their blog posts I think I would still be wracking my brain trying to figure all this out.

I’m sure I’m still out of date with my methodology but at least it’s all working. I will be looking into some alternative methods such as git-s3 to try and see if that is an easier way of getting my files onto s3.
