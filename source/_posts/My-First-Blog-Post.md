---
title: My First Blog Post
tags:
year: 2016
date: 2016-07-15 09:04:00
---

Welcome to my website. I have decided it was time to move away from Wordpress and the web host I was using. In my quest for something different I came across [Hexo](https://hexo.io) and seemed up my alley as a lightweight blog platform. Wordpress was just too much for the minimal blogging I do, at the very least this will be a nice little experiment.

I still have to figure a few things out with this site. I am using AWS to host this site as opposed to a standard webhost so I have to figure out how to get posts on here fairly easily. I am going to be spending the next day or so looking into ways to upload from git to S3. It doesn't seem impossible, but we will see.
