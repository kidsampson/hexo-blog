module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        aws: grunt.file.readJSON('../grunt-aws.json'),

        shell: {
            clean: {
                command: 'hexo clean'
            },
            generate: {
                command: 'hexo generate'
            },
            server: {
                command: 'hexo server'
            }
        },
        robotstxt: {
            dev: {
                dest: 'public/',
                policy: [{
                    ua: '*',
                    disallow: '/'
                }, {
                    sitemap: ['http://localhost:4000/sitemap.xml']
                }, {
                    host: 'localhost:4000'
                }]
            },
            staging: {
                dest: 'public/',
                policy: [{
                    ua: '*',
                    disallow: '/'
                }, {
                    sitemap: ['http://localhost:4000/sitemap.xml']
                }, {
                    host: 'localhost:4000'
                }]
            },
            production: {
                dest: 'public/',
                policy: [{
                    ua: '*',
                    allow: '/'
                }, {
                    sitemap: ['http://jacobnichols.me/sitemap.xml']
                }, {
                    host: 'jacobnichols.me'
                }]
            }
        },
        sitemap: {
            dev: {
                siteRoot: 'public/',
                homepage: 'http://localhost:4000'
            },
            staging: {
                siteRoot: 'public/',
                homepage: 'http://staging.jacobnichols.me'
            },
            production: {
                siteRoot: 'public/',
                homepage: 'http://jacobnichols.me'
            }
        },
        s3: {
            options: {
                accessKeyId: '<%= aws.accessIdProduction %>',
                secretAccessKey: '<%= aws.accessKeyProduction %>',
                bucket: '<%= aws.bucketProduction %>',
                access: 'public-read',
                headers: {
                    // Two Year cache policy (1000 * 60 * 60 * 24 * 730)
                    "Cache-Control": "max-age=630720000, public",
                    "Expires": new Date(Date.now() + 63072000000).toUTCString()
                }
            },
            public: {
                cwd: "public/",
                src: "**"
            }
        }
    });

    //Load NPM tasks
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-robots-txt')
    grunt.loadNpmTasks('grunt-sitemap');
    grunt.loadNpmTasks('grunt-aws');

    //Grunt tasks
    grunt.registerTask('default', ['shell:clean', 'shell:generate', 'sitemap:dev', 'robotstxt:dev', 'shell:server']);
    //grunt.registerTask('staging', ['shell:clean','shell:generate','sitemap:staging', 'robotstxt:staging', 's3:staging']);
    grunt.registerTask('deploy', ['shell:clean', 'shell:generate', 'sitemap:production', 'robotstxt:production', 's3']);
};
